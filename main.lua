require "torch"
require "nn"
require "math"

-- global variables
learningRate = 0.012
maxIterations = 500


-- here we set up the architecture of the neural network
function create_network(nb_outputs)
	local numberOfFeatureMaps = 3
	local kernelSize = 3
	local neuronsRow = 8
	local convResult = neuronsRow - (kernelSize-1)
	local subSamplingRes = convResult / 2
   local ann = nn.Sequential();  -- make a multi-layer structure
   
   -- input is 8x8x1                        
   ann:add(nn.SpatialConvolution(1,numberOfFeatureMaps,kernelSize,kernelSize))  
--becames 6x6x6 
-- numberOfInputPlane, numberOfOutputPlane,kernelWidth,kernelHeight
--the default step of convolution is 1
--padding can be added
   ann:add(nn.SpatialSubSampling(numberOfFeatureMaps,2,2,2,2))
	--ann:add(nn.SpatialAveragePooling(2,2,2,2))
-- becomes  3x3x6
--numberOfinputPlanes,kernelWidth,kernelHeight,stepOfSubsamplingWidth,stepOfSubsamplingHeight

--i can try also nn.SpatialAveragePooling(kW,kH)
      
   ann:add(nn.Reshape(subSamplingRes*subSamplingRes*numberOfFeatureMaps)) --becames 3*3*6 vector of neurons
   ann:add(nn.Tanh()) --apply ativation function

   ann:add(nn.Linear(subSamplingRes*subSamplingRes*numberOfFeatureMaps,nb_outputs)) --fully connected layer
   
   --Torch initializes the weights with a uniform distribution that takes into account the fanin/fanout of the model. 

   return ann
end

-- train a Neural Netowrk
function train_network( network, dataset,datasetClasses)
               
   print( "Training the network" )
   local criterion = nn.MSECriterion()--negative log-likelihood
--nn.MSECriterion() --mean squared error
   
   for iteration=1,maxIterations do --for maxIteration times
	for index = 1,dataset:size(1) do -- for each data in the training      
     	 local input = dataset[index]               
      	 local output = datasetClasses[index]
      
    	  criterion:forward(network:forward(input), output)
      
    	  network:zeroGradParameters() --reset parameters before backward call
     	 network:backward(input, criterion:backward(network.output, output))
   	   network:updateParameters(learningRate)
  	 end
    end

end

function test_predictor(predictor, test_dataset,testing_target_classes, classes, classes_names)

        local mistakes = torch.Tensor(test_dataset:size(1)):fill(0)
        local tested_samples = 0
        
        --print( "----------------------" )
        --print( "Index Label Prediction" )
        for i=1,test_dataset:size(1) do

               local input  = test_dataset[i]
               local class_id = testing_target_classes[i]
        
               local responses_per_class  =  predictor:forward(input) 
               local probabilites_per_class = torch.exp(responses_per_class)
               local probability, prediction = torch.max(probabilites_per_class, 1) 

                      
               if prediction[1] ~= class_id then
                      mistakes[i] = mistakes[i] + 1
                      local label = classes_names[ classes[class_id] ]
                      local predicted_label = classes_names[ classes[prediction[1] ] ]
                     -- print(i , label , predicted_label )
		else
		local label = classes_names[ classes[class_id] ]
                      local predicted_label = classes_names[ classes[prediction[1] ] ]
                      --print(i , label , predicted_label )
               end

               tested_samples = tested_samples + 1
        end

        --local test_err = mistakes[i]/tested_samples
        --print ( "Test error " .. test_err .. " ( " .. mistakes .. " out of " .. tested_samples .. " )")
	return mistakes
end

function robustnessTest(network, training_dataset, training_correct_class_id, classes, classes_names)
	local runs = 1000
	for noiseBits = 0,15 do
		print()
		print("Noise bits = "..noiseBits)
		local  mistakes = torch.Tensor(training_correct_class_id:size(1)):fill(0)
		for i =1, runs do
			--corrupt data
			local corruptedDataset = insertNoise(training_dataset,noiseBits)
			--test
			 mistakes = mistakes + test_predictor(network, corruptedDataset,training_correct_class_id, classes, classes_names)
		end

		for i=1, training_correct_class_id:size(1)do
			local test_err = mistakes[i]/(runs * (training_correct_class_id:size(1))) * 100
	       		print ( "Success rate class " ..i.." ".. 100-test_err)
		end
	end
 	

end

function insertNoise(training_dataset,noiseBits)
	local tmpData = training_dataset:clone()

	for input=1,training_dataset:size(1) do
		for i = 1, noiseBits do
			--generate random indices
			local k = math.random(tmpData:size(3))
			local h = math.random(tmpData:size(3))

			if tmpData[input][1][k][h] == 1 then
			  tmpData[input][1][k][h] = 0
			else		
			  tmpData[input][1][k][h] = 1
			end			
		end
	end

	return tmpData
end

function trainAndTest(network, training_dataset, training_target_classes,training_correct_class_id, classes, classes_names,testing_dataset,testing_target_classes)	
        
	local mistakes = torch.Tensor(testing_dataset:size(1)):fill(0)
	local mistakesTraining = torch.Tensor(training_dataset:size(1)):fill(0)
	local total_tests = 5

	for i = 1,total_tests do		
	        train_network(network, training_dataset, training_target_classes)
        
		mistakesTraining = mistakesTraining + test_predictor(network, training_dataset, training_correct_class_id,classes, classes_names)
    		mistakes = mistakes + test_predictor(network, testing_dataset,testing_target_classes,classes, classes_names)
	end
	
	for i=1, training_dataset:size(1) do
		local test_err_train = mistakesTraining[i]/(total_tests*9) * 100
		print  ( "Test error training " .. 100 - test_err_train .. " ( " .. mistakesTraining[i] .. " out of " .. total_tests*9 .. " )")
	end

	for i=1, testing_dataset:size(1) do
		local test_err = mistakesTraining[i]/(total_tests*3) * 100
		print  ( "Test error test " .. 100 - test_err .. " ( " .. mistakes[i] .. " out of " .. total_tests*3 .. " )")
	end


	
end

function getStats(network, training_dataset, training_target_classes)
	for i=1,10 do
	 train_network(network, training_dataset, training_target_classes)
	end
end

-- main routine
function main()

      
local training_dataset, 
training_target_classes,
training_correct_class_id,
testing_dataset, 
testing_target_classes,
classes, 
classes_names = dofile('loadDataset.lua')

--create net
local network = create_network(#classes) --#classes is the number of classes

--trainAndTest(network, training_dataset, training_target_classes,training_correct_class_id, classes, classes_names,testing_dataset,testing_target_classes)

train_network(network, training_dataset, training_target_classes)

print ("Robustness test")
robustnessTest(network, training_dataset, training_correct_class_id, classes, classes_names)

--getStats(network, training_dataset,training_target_classes)

end


main()
