
require "torch"
require "math"

local classes = {1,2,3,4} -- indices in torch/lua start at 1, not at zero
local classes_names = {'A','B','C','Other'}

local training_dataset = torch.Tensor({
               {{{0, 0, 0, 1, 1, 0, 0, 0},
                {0, 0, 1, 0, 0, 1, 0, 0}, -- ‘A’
                {0, 1, 0, 0, 0, 0, 1, 0},
               {1, 0, 0, 0, 0, 0, 0, 1},
                {1, 1, 1, 1, 1, 1, 1, 1},
                {1, 0, 0, 0, 0, 0, 0, 1},
                {1, 0, 0, 0, 0, 0, 0, 1},
                {1, 0, 0, 0, 0, 0, 0, 1}}},
               {{{1, 1, 1, 1, 1, 1, 1, 0},
               {1, 0, 0, 0, 0, 0, 0, 1},-- ‘B’
                {1, 0, 0, 0, 0, 0, 1, 0},
                {1, 1, 1, 1, 1, 1, 0, 0},
                {1, 0, 0, 0, 0, 0, 1, 0},
                {1, 0, 0, 0, 0, 0, 0, 1},
                {1, 0, 0, 0, 0, 0, 1, 0},
                {1, 1, 1, 1, 1, 1, 0, 0}}},
               {{{0, 0, 1, 1, 1, 1, 1, 1}, -- ‘C’
                {0, 1, 0, 0, 0, 0, 0, 0},
               {1, 0, 0, 0, 0, 0, 0, 0},
                {1, 0, 0, 0, 0, 0, 0, 0},
                {1, 0, 0, 0, 0, 0, 0, 0},
                {1, 0, 0, 0, 0, 0, 0, 0},
                {0, 1, 0, 0, 0, 0, 0, 0},
                {0, 0, 1, 1, 1, 1, 1, 1}}},
               {{{1, 1, 1, 1, 1, 1, 0, 0}, -- ‘D’
                {1, 0, 0, 0, 0, 0, 1, 0},
                {1, 0, 0, 0, 0, 0, 0, 1},
                {1, 0, 0, 0, 0, 0, 0, 1},
               {1, 0, 0, 0, 0, 0, 0, 1},
                {1, 0, 0, 0, 0, 0, 0, 1},
                {1, 0, 0, 0, 0, 0, 1, 0},
                {1, 1, 1, 1, 1, 1, 0, 0}}},
              { {{1, 1, 1, 1, 1, 1, 1, 1}, -- ‘E’
                {1, 0, 0, 0, 0, 0, 0, 0},
                {1, 0, 0, 0, 0, 0, 0, 0},
                {1, 1, 1, 1, 1, 1, 1, 1},
                {1, 0, 0, 0, 0, 0, 0, 0},
                {1, 0, 0, 0, 0, 0, 0, 0},
                {1, 0, 0, 0, 0, 0, 0, 0},
                {1, 1, 1, 1, 1, 1, 1, 1}}},
              { {{1, 1, 1, 1, 1, 1, 1, 1},-- ‘F’
                {1, 0, 0, 0, 0, 0, 0, 0},
                {1, 0, 0, 0, 0, 0, 0, 0},
                {1, 1, 1, 1, 1, 1, 1, 1},
                {1, 0, 0, 0, 0, 0, 0, 0},
                {1, 0, 0, 0, 0, 0, 0, 0},
                {1, 0, 0, 0, 0, 0, 0, 0},
                {1, 0, 0, 0, 0, 0, 0, 0}}},
              { {{0, 0, 1, 1, 1, 1, 1, 1},-- ‘G’
                {0, 1, 0, 0, 0, 0, 0, 0},
                {1, 0, 0, 0, 0, 0, 0, 0},
                {1, 0, 0, 0, 0, 0, 0, 0},
                {1, 0, 1, 1, 1, 1, 1, 1},
                {1, 0, 0, 0, 0, 0, 0, 1},
                {0, 1, 0, 0, 0, 0, 0, 1},
                {0, 0, 1, 1, 1, 1, 1, 1}}},
               {{{1, 0, 0, 0, 0, 0, 0, 1},
                {1, 0, 0, 0, 0, 0, 0, 1}, -- ‘H’
                {1, 0, 0, 0, 0, 0, 0, 1},
                {1, 1, 1, 1, 1, 1, 1, 1},
                {1, 0, 0, 0, 0, 0, 0, 1},
                {1, 0, 0, 0, 0, 0, 0, 1},
                {1, 0, 0, 0, 0, 0, 0, 1},
                {1, 0, 0, 0, 0, 0, 0, 1}}},
               {{{0, 0, 1, 1, 1, 1, 1, 0}, -- I
                {0, 0, 0, 0, 1, 0, 0, 0},
                {0, 0, 0, 0, 1, 0, 0, 0},
                {0, 0, 0, 0, 1, 0, 0, 0},
                {0, 0, 0, 0, 1, 0, 0, 0},
                {0, 0, 0, 0, 1, 0, 0, 0},
                {0, 0, 0, 0, 1, 0, 0, 0},
                {0, 0, 1, 1, 1, 1, 1, 0}}}
})

local training_target_classes = torch.Tensor({
{1,0,0,0},
{0,1,0,0},
{0,0,1,0},
{0,0,0,1},
{0,0,0,1},
{0,0,0,1},
{0,0,0,1},
{0,0,0,1},
{0,0,0,1}})

local training_correct_class_id = torch.Tensor({1,2,3,4,4,4,4,4,4})

local testing_dataset = torch.Tensor({
	       {{{1, 0, 0, 0, 0, 0, 0, 1},
                {0, 1, 0, 0, 0, 0, 1, 0}, -- ‘X’
                {0, 0, 1, 0, 0, 1, 0, 0},
                {0, 0, 0, 1, 1, 0, 0, 0},
                {0, 0, 0, 1, 1, 0, 0, 0},
                {0, 0, 1, 0, 0, 1, 0, 0},
                {0, 1, 0, 0, 0, 0, 1, 0},
                {1, 0, 0, 0, 0, 0, 0, 1}}},
               {{{0, 1, 0, 0, 0, 0, 0, 1},
                {0, 0, 1, 0, 0, 0, 1, 0}, -- ‘Y’
                {0, 0, 0, 1, 0, 1, 0, 0},
                {0, 0, 0, 0, 1, 0, 0, 0},
                {0, 0, 0, 0, 1, 0, 0, 0},
                {0, 0, 0, 0, 1, 0, 0, 0},
                {0, 0, 0, 0, 1, 0, 0, 0},
                {0, 0, 0, 0, 1, 0, 0, 0}}},
               {{{1, 1, 1, 1, 1, 1, 1, 1},-- ‘Z’
                {0, 0, 0, 0, 0, 0, 1, 0},
                {0, 0, 0, 0, 0, 1, 0, 0},
                {0, 0, 0, 0, 1, 0, 0, 0},
                {0, 0, 0, 1, 0, 0, 0, 0},
                {0, 0, 1, 0, 0, 0, 0, 0},
                {0, 1, 0, 0, 0, 0, 0, 0},
                {1, 1, 1, 1, 1, 1, 1, 1}}}
})

local testing_target_classes = torch.Tensor({4,4,4})

return training_dataset, 
training_target_classes,
training_correct_class_id,
testing_dataset, 
testing_target_classes,
classes, 
classes_names
















